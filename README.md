Myuzi is a free and open source Linux app for streaming music from YouTube. It has no ads and does not require an account.

Uses SponsorBlock data licensed used under CC BY-NC-SA 4.0 from https://sponsor.ajay.app/.

Copyright © 2022 zehkira, [MIT License](https://gitlab.com/zehkira/myuzi/-/blob/master/source/LICENSE).

<br><a href='https://gitlab.com/zehkira/myuzi/-/blob/master/INSTALL.md#install-package'>
    <img src='https://gitlab.com/zehkira/myuzi/-/raw/master/assets/download.png' alt='install from aur' width='220'>
</a>
&nbsp;
<a href='https://www.patreon.com/bePatron?u=65739770'>
    <img src='https://gitlab.com/zehkira/myuzi/-/raw/master/assets/support.png' alt='patreon' width='220'>
</a>
<br><br>
<img src='https://gitlab.com/zehkira/myuzi/-/raw/master/assets/screenshot1.png' alt='screenshot' width='680'>
