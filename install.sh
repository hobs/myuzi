sudo apt-get install -y gtk-4-examples python3-wxgtk4.0 yt-dlp gstreamer1.0-libav \
	libgstreamer-plugins-good1.0-dev libgstreamer-plugins-good1.0 \
	yt-dlp gstreamer1.0-libav

python -m virtualenv venv
source venv/bin/activate

pip install -r requirements.txt
