msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"

#: ../myuzi/app.py:138
msgid "Searching for a song to play..."
msgstr ""

#: ../myuzi/app.py:207
msgid "Playback error"
msgstr ""

#: ../myuzi/app.py:412
msgid "Downloading song..."
msgstr ""

#: ../myuzi/app.py:425 ../myuzi/app.py:589
msgid "Download error"
msgstr ""

#: ../myuzi/app.py:426
msgid "Song download failed"
msgstr ""

#: ../myuzi/app.py:480
msgid "Playlist creation failed"
msgstr ""

#: ../myuzi/app.py:481 ../myuzi/app.py:527
msgid "Playlist already exists"
msgstr ""

#: ../myuzi/app.py:505
msgid "Playlist import failed"
msgstr ""

#: ../myuzi/app.py:506
msgid "No playlist data in clipboard"
msgstr ""

#: ../myuzi/app.py:526
msgid "Playlist rename failed"
msgstr ""

#: ../myuzi/app.py:546
msgid "Exported"
msgstr ""

#: ../myuzi/app.py:547
msgid "Playlist data copied to clipboard"
msgstr ""

#: ../myuzi/app.py:569
msgid "Downloading playlist..."
msgstr ""

#: ../myuzi/app.py:590
msgid "Playlist download failed"
msgstr ""

#: ../myuzi/app.py:636
msgid "Unavailable"
msgstr ""

#: ../myuzi/app.py:637
msgid "Can't search in offline mode"
msgstr ""

#: ../myuzi/app.py:656
msgid "Searching..."
msgstr ""

#: ../myuzi/app.py:676
msgid "Can't switch to online mode"
msgstr ""

#: ../myuzi/app.py:677
msgid "Connection failed"
msgstr ""

#: ../myuzi/app.py:702
msgid "translator-credits"
msgstr ""

#: ../myuzi/app.py:703
msgid "Patrons"
msgstr ""

#: ../myuzi/app.py:705
msgid "Donate"
msgstr ""

#: ../myuzi/app.py:806 ../myuzi/app.py:1062
msgid "New playlist..."
msgstr ""

#: ../myuzi/app.py:812 ../myuzi/app.py:936
msgid "Add to queue"
msgstr ""

#: ../myuzi/app.py:871
msgid "Delete"
msgstr ""

#: ../myuzi/app.py:874 ../myuzi/app.py:952
msgid "Remove from local storage"
msgstr ""

#: ../myuzi/app.py:877
msgid "Export to clipboard"
msgstr ""

#: ../myuzi/app.py:887 ../myuzi/app.py:957
msgid "Save to local storage"
msgstr ""

#: ../myuzi/app.py:942
msgid "Move up"
msgstr ""

#: ../myuzi/app.py:947
msgid "Move down"
msgstr ""

#: ../myuzi/app.py:963
msgid "Remove from playlist"
msgstr ""

#: ../myuzi/app.py:1076
msgid "Remove from queue"
msgstr ""

#: ../myuzi/app.py:1081
msgid "Volume"
msgstr ""

#: ../myuzi/app.py:1102
msgid "Autoplay similar songs after queue ends"
msgstr ""

#: ../myuzi/app.py:1179
msgid "Playlists"
msgstr ""

#: ../myuzi/app.py:1180
msgid "Search"
msgstr ""

#: ../myuzi/app.py:1186
msgid "Online"
msgstr ""

#: ../myuzi/app.py:1195
msgid "Import"
msgstr ""

#: ../myuzi/app.py:1200
msgid "About"
msgstr ""
