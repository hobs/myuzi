<a href='https://aur.archlinux.org/packages/myuzi'>
    <img src='https://gitlab.com/zehkira/myuzi/-/raw/master/assets/install-aur.png' alt='install from aur' width='220'>
</a>
&nbsp;
<a href='https://flathub.org/apps/details/com.gitlab.zehkira.Myuzi'>
    <img src='https://gitlab.com/zehkira/myuzi/-/raw/master/assets/install-flathub.png' alt='install from flathub' width='220'>
</a>

<hr>

Or install from source by following the steps below:

Required packages:
- [Gtk 4](https://www.gtk.org/)
- [yt-dlp](https://github.com/yt-dlp/yt-dlp)
- [GStreamer](https://github.com/GStreamer/gstreamer)
    - [gst-libav](https://github.com/GStreamer/gst-libav)
    - [gst-plugins-good](https://github.com/GStreamer/gst-plugins-good)
- [Python 3](https://www.python.org/)
    - [BeautifulSoup4](https://pypi.org/project/beautifulsoup4/)
    - [PyGObject](https://pygobject.readthedocs.io/en/latest/getting_started.html#getting-started)
    - [Requests](https://pypi.org/project/requests/)
    - [Setuptools](https://pypi.org/project/setuptools/)

Install:

```
$ git clone https://gitlab.com/zehkira/myuzi.git
$ cd myuzi/source
$ pip install .
# make install
```

Unistall:

```
$ pip uninstall myuzi
# make uninstall
```
